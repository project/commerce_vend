<?php

/**
 * @file
 * Handle the admin side of Commerce Vend.
 */


/**
 * Implements hook_form().
 */
function commerce_vend_admin_form($form, $form_state) {
  $form = array();
  $form['credentials'] = array(
    '#type' => 'fieldset',
    '#title' => 'Credentials',
    'children' => array(
      'commerce_vend_instance' => array(
        '#type' => 'textfield',
        '#title' => 'Vend Instance URL',
        '#default_value' => commerce_vend_variable_get('instance'),
      ),
      'commerce_vend_username' => array(
        '#type' => 'textfield',
        '#title' => 'Vend Username',
        '#default_value' => commerce_vend_variable_get('username'),
      ),
      'commerce_vend_password' => array(
        '#type' => 'textfield',
        '#title' => 'Vend Password',
        '#default_value' => commerce_vend_variable_get('password'),
      ),
      'commerce_vend_test_connection' => array(
        '#type' => 'submit',
        '#value' => 'Test Connection',
        '#submit' => array(
          'commerce_vend_validate_credentials',
        ),
        '#disabled' => commerce_vend_variable_get('instance') == commerce_vend_default_variables()['commerce_vend_instance'],
      ),
    ),
  );

  $form['configuration'] = array(
    '#type' => 'fieldset',
    '#title' => 'Configuration',
    'children' => array(
      'commerce_vend_verbose_logging' => array(
        '#type' => 'checkbox',
        '#title' => t('Show admin users verbose integration messages when managing products.'),
        '#default_value' => commerce_vend_variable_get('verbose_logging'),
      ),
      'commerce_vend_enable_order_sync' => array(
        '#type' => 'checkbox',
        '#title' => t('Sync orders from Vend back into Drupal Commerce as orders.'),
        '#default_value' => commerce_vend_variable_get('enable_order_sync'),
      ),
    ),
  );

  return system_settings_form($form);
}


/**
 * Check that the credentials entered by the user are correct.
 */
function commerce_vend_validate_credentials($form, &$form_state) {

  $product_list = (new CommerceVendProductList())->execute();

  if ($product_list->wasSuccess()) {
    drupal_set_message(t('Successfully made a connection to the Vend instance.'));
  }
  else {
    drupal_set_message(t('Could not connect to the Vend instance.'), 'error');
  }

}
