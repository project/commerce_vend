<?php

/**
 * @file
 * Handle the syncing of sales from Vend into Commerce orders.
 */


/**
 * Sync orders from Vend into Drupal commerce.
 */
function commerce_vend_sync_orders() {

  // Get all of the sales since the last time we synced them back.
  $last_sync = commerce_vend_variable_get('last_order_sync');
  $sales = (new CommerceVendRegisterSales(array(
    'since' => $last_sync,
  )))->execute();

  // Check we were able to get a result.
  if (!$sales->wasSuccess()) {
    commerce_vend_watchdog_set_message('Unable to sync orders from Commerce Vend, given error @error', array('@error' => $sales->getError()), 'error');
    return;
  }

  // Loop through each of the sales made.
  foreach ($sales->getResponse()['register_sales'] as $sale) {
    commerce_vend_create_order_from_remote_sale($sale);
  }

  // Ensure next time the sync runs, it only grabs new orders.
  commerce_vend_variable_set('last_order_sync', gmdate('c'));
}


/**
 * Create a local Drupal order from a sale tracked in Vend.
 *
 * @param array $sale
 *   A sale array from Vend.
 */
function commerce_vend_create_order_from_remote_sale($sale) {
  // @todo, currently all orders from Vend beyond to Anon users. We might be
  // able to create user accounts for customers, but this would have to be
  // evaluated for usefulness.
  $order = commerce_order_new(0, 'completed');
  // Store the raw sale data on our order for later inspection if required.
  $order->data = $sale;
  commerce_order_save($order);
  $order_wrapper = entity_metadata_wrapper('commerce_order', $order);

  // Loop through all the products the user purchased.
  foreach ($sale['register_sale_products'] as $sale_item) {

    // Create a line item for each product the user purchased.
    $product = commerce_product_load_by_sku($sale_item['sku']);

    if (!$product) {
      commerce_vend_watchdog_set_message('Unable to load local product from remote Vend product, given SKU @sku.', array('@sku' => $sale_item['sku']), 'error');
      continue;
    }

    // Save our line item and add it to the order.
    $line_item = commerce_product_line_item_new($product, $sale_item['quantity'], $order->order_id);
    commerce_line_item_save($line_item);
    $order_wrapper->commerce_line_items[] = $line_item;

    // Reduce the stock levels by the amount found on the order.
    // @todo, this should probably be configurable or moved into rules.
    if (module_exists('commerce_stock')) {
      // Reduce the local stock levels to match Vend.
    }

  }

  // Save the order again to ensure the line items are saved.
  commerce_order_save($order);
  commerce_vend_watchdog_set_message('Successfully created order @order_id from Vend order @vend_id', array('@order_id' => $order->order_id, '@vend_id' => $sale['id']));
}
