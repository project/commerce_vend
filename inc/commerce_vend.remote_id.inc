<?php

/**
 * @file
 * Create and manage a field to store our remote ID on all of the commerce
 * product entities.
 */


// The name of the field used to store our remote ID.
define('COMMERCE_VEND_REMOTE_ID_FIELD_NAME', 'commerce_vend_id');


/**
 * Implements hook_enable().
 */
function commerce_vend_enable() {
  // When our module is enabled, ensure our ID field is created.
  commerce_vend_update_id_field_instances();
}


/**
 * Implements hook_commerce_product_type_insert().
 */
function commerce_vend_commerce_product_type_insert() {
  // When product types are create, ensure our ID field is added.
  module_load_include('module', 'commerce_product');
  commerce_product_types_reset();
  commerce_vend_update_id_field_instances();
}


/**
 * Implements hook_field_widget_form_alter().
 */
function commerce_vend_field_widget_form_alter(&$element, &$form_state, $context) {
  // Nobody needs to see the remote ID when accessing products.
  if ($context['field']['field_name'] == COMMERCE_VEND_REMOTE_ID_FIELD_NAME) {
    $element['#access'] = FALSE;
  }
}


/**
 * Update the product types to include a remote ID field that we can use.
 */
function commerce_vend_update_id_field_instances() {
  module_load_include('module', 'commerce');
  // Attach to all our product types.
  foreach (commerce_product_types() as $type => $product_type) {
    commerce_vend_create_id_field_instance(COMMERCE_VEND_REMOTE_ID_FIELD_NAME, $type);
  }
}


/**
 * Ensure our ID field has been created for the given product type.
 */
function commerce_vend_create_id_field_instance($field_name, $bundle) {

  $entity_type = 'commerce_product';
  $label = t('Commerce Vend ID');

  $field = field_info_field($field_name);
  $instance = field_info_instance($entity_type, $field_name, $bundle);

  // Check if the field exists at all.
  if (empty($field)) {
    $field = array(
      'field_name' => $field_name,
      'type' => 'text',
      'cardinality' => 1,
      'entity_types' => array($entity_type),
      'translatable' => FALSE,
      // Keep this locked so admins cannot tamper with our remote ID field.
      'locked' => TRUE,
    );
    $field = field_create_field($field);
  }

  // Check if we have the field on our given bundle.
  if (empty($instance)) {
    $instance = array(
      'field_name' => $field_name,
      'entity_type' => $entity_type,
      'bundle' => $bundle,
      'label' => $label,
      'required' => FALSE,
      'settings' => array(),
      'widget' => array(
        'type' => 'text_textfield',
      ),
    );
    field_create_instance($instance);
  }
}
