<?php

/**
 * @file
 * Product related CRUD tasks for keeping Drupal commerce and Vend in sync.
 */

// The name of the hook used to alter products.
define('COMMERCE_VEND_PRODUCT_MAPPING_HOOK_NAME', 'commerce_vend_product_mapping');


/**
 * Sync a single product entity to Vend.
 *
 * @param stdClass $product
 *   The product to sync.
 * @param array $context
 *   The hook_action_info context.
 */
function commerce_vend_sync_product($product, $context = array()) {

  $price = commerce_product_calculate_sell_price($product);

  // Create a mapping between our product and the remote Vend product.
  $product_mapping = array(
    // Some basic information about a product.
    'source_variant_id' => $product->product_id,
    'type' => $product->type,
    'sku' => $product->sku,
    'name' => $product->title,
    // @todo, Do not fully understand the impact of this field yet.
    'handle' => $product->sku,
    // Since commerce can have a variety of pricing rules, let the website
    // decide how something should be priced and then relay the final price to
    // the POS system.
    'retail_price' => ($price['amount'] / 100),
    'display_retail_price_tax_inclusive' => '1',
  );

  // Send the existing product ID if it exists.
  $existing_remote_id = field_get_items('commerce_product', $product, COMMERCE_VEND_REMOTE_ID_FIELD_NAME);
  if ($existing_remote_id) {
    $product_mapping['id'] = $existing_remote_id[0]['value'];
  }

  // If we have installed commerce_stock, send that across too.
  $stock_level = field_get_items('commerce_product', $product, 'commerce_stock');
  if ($stock_level) {

    $product_mapping['inventory'][] = array(
      // @todo, Main Outlet is simply the default outlet that comes with Vend.
      'outlet_name' => 'Main Outlet',
      'count' => $stock_level[0]['value'],
      // @todo, add these as fields on the product or create a configuration.
      'reorder_point' => 10,
      'restock_level' => 10,
    );
  }

  // Allow people to override the product mapping.
  foreach (module_implements(COMMERCE_VEND_PRODUCT_MAPPING_HOOK_NAME) as $module) {
    call_user_func($module . '_' . COMMERCE_VEND_PRODUCT_MAPPING_HOOK_NAME, $product_mapping, $product);
  }

  // Create a product write object.
  $product_sync = new CommerceVendWriteProduct($product_mapping);

  // Execute the write operation.
  $response = $product_sync->execute();

  // If our API request failed, raise an error with the user.
  if (!$response->wasSuccess()) {
    commerce_vend_watchdog_set_message('Failed to sync Vend product with response: @response', array('@response' => $response->getError()));
    return;
  }

  // Get the response from our API.
  $response_info = $response->getResponse();

  // Report that no product ID was returned from the API.
  if (!isset($response_info['product']['id'])) {
    commerce_vend_watchdog_set_message('Commerce Vend could not retrieve the remote Id for product @pid', array('@pid' => $product->product_id));
    return;
  }

  // If the response contained a product ID (like it always should).
  $vend_id = $response_info['product']['id'];

  // Store the commerce remote ID.
  $product->{COMMERCE_VEND_REMOTE_ID_FIELD_NAME}[$product->language][0]['value'] = $vend_id;
  field_attach_update('commerce_product', $product);

  commerce_vend_watchdog_set_message(
    'Successfully synced product #@product_id to @vend_id',
    array(
      '@product_id' => $product->product_id,
      '@vend_id' => $vend_id,
    ),
    'status'
  );

}


/**
 * Delete a product on vend based on our local product.
 *
 * @param stdClass $local_product
 *   The local product we are deleting the remote product for.
 */
function commerce_vend_delete_remote_product($local_product) {
  // Get the vend product ID and delete it.
  $vend_product_id = field_get_items('commerce_product', $local_product, 'commerce_vend_id');
  if ($vend_product_id) {
    $vend_id = $vend_product_id[0]['value'];
    $deletion = new CommerceVendDeleteProduct($vend_id);
    $deletion->execute();
    commerce_vend_watchdog_set_message(
      'Deleted Vend product @vend_id because product #@product_id has been deleted.',
      array(
        '@vend_id' => $vend_id,
        '@product_id' => $local_product->product_id,
      ),
      'status'
    );
  }
}

/**
 * Sync all the products from Drupal Commerce to Vend.
 */
function commerce_vend_sync_all_products() {

}
