<?php

/**
 * @file
 * Request the sales made on a register.
 */

/**
 * Class CommerceVendRegisterSales
 */
class CommerceVendRegisterSales extends CommerceVendRequest {

  /**
   * Write a product to the vend database.
   */
  public function __construct($filters = array()) {

    // Set our endpoint info.
    $this->setRequestMethod('GET');
    $this->setRequestEndpoint('/api/register_sales');

    // Set our attributes for product creation.
    $this->setRequestParams($filters);
  }
};
