<?php
/**
 * @file
 * An request object for retrieving Vend products.
 */

/**
 * Class VendProductRequest
 *
 * See API documentation: http://docs.vendhq.com/api/0.x/products.html
 */
class CommerceVendProductList extends CommerceVendRequest {

  /**
   * Gather the information we need to do a product listing.
   *
   * @param array $filters
   *   An array of filters to use in our query.
   */
  public function __construct($filters = array()) {

    // Set our endpoints to be the product listing.
    $this->setRequestEndpoint('/api/products');
    $this->setRequestMethod('GET');

    // Set our filters.
    $this->setRequestParams($filters);

  }

};
