<?php
/**
 * @file
 * A base class for CommerceVend.
 */

/**
 * Class CommerceVendBase
 */
abstract class CommerceVendBase {

  /**
   * Returns the authentication string for the Vend API.
   *
   * @return string
   *   The authentication string.
   */
  protected function getAuthString() {
    return commerce_vend_variable_get('username') . ':' . commerce_vend_variable_get('password');
  }

  /**
   * Get the Vend API URL.
   *
   * @return string
   *   The URL string if it exists or null.
   */
  protected function getAppUrl() {
    return commerce_vend_variable_get('instance');
  }

};
