<?php
/**
 * @file
 * An object representing a Vend API request.
 */

/**
 * Class CommerceVendRequest
 *
 * Make requests to the Vend API.
 */
abstract class CommerceVendRequest extends CommerceVendBase {

  // The required properties to make a request.
  protected $requestMethod = FALSE;
  protected $requestEndpoint = FALSE;

  // The information we pass to the API.
  protected $requestBody = array();
  protected $requestParams = array();

  // Vend can paginate their results.
  protected $currentPage = 1;


  /**
   * Execute the request and get the response object.
   *
   * @return \CommerceVendResponse
   *   The response object.
   */
  public function execute() {
    return $this->processRequest();
  }


  /**
   * Process a request to Vend.
   */
  protected function processRequest() {

    $target_url = 'https://' . $this->getAuthString() . '@' . $this->getAppUrl() . $this->requestEndpoint;

    // Build a list of request options to pass to drupal_http_request().
    $request_options = array(
      'headers' => array(),
      'method' => $this->requestMethod,
    );

    // Ensure we have an endpoint and request method.
    if (!$this->requestEndpoint || !$this->requestMethod) {
      throw new Exception('Incorrect endpoint or request method.');
    }

    // Ensure the pagination works.
    if ($this->getPage() > 1) {
      $this->requestParams['page'] = $this->getPage();
    }

    // If we are using request params.
    if (count($this->requestParams) > 0) {
      $target_url = url($target_url, array('query' => $this->requestParams));
    }

    // If we are using a JSON request body.
    if (count($this->requestBody) > 0) {
      // Setup the request to POST json.
      $request_options['headers']['Content-Type'] = 'application/json';
      $request_options['data'] = drupal_json_encode($this->requestBody);
    }

    // Post to Vend and return a response object.
    $response = drupal_http_request($target_url, $request_options);
    return new CommerceVendResponse($response);
  }

  /**
   * Go to the next page of a query.
   */
  public function nextPage() {
    $this->setPage($this->getPage() + 1);
    return $this->processRequest();
  }

  /**
   * Set the page of results.
   *
   * @param int $page
   *   The numerical page value.
   *
   * @throws Exception
   */
  public function setPage($page) {
    if (!is_int($page)) {
      throw new Exception('Tried to set the pager to a non-integer value.');
    }
    $this->currentPage = $page;
  }

  /**
   * Get the current page we are on.
   *
   * @return int
   *   The current page.
   */
  public function getPage() {
    return $this->currentPage;
  }

  /**
   * Set the request endpoint.
   *
   * @param string $endpoint
   *   The endpoint to use for this request.
   */
  protected function setRequestEndpoint($endpoint) {
    $this->requestEndpoint = $endpoint;
  }

  /**
   * Set the request method.
   *
   * @param string $method
   *   The method to use for this request.
   */
  protected function setRequestMethod($method) {
    $this->requestMethod = $method;
  }

  /**
   * Sent any parameters for the request.
   *
   * @param array $request_params
   *   An array of parameters.
   */
  protected function setRequestParams($request_params) {
    $this->requestParams = $request_params;
  }

  /**
   * Set the request body to be sent.
   *
   * @param string $request_body
   *   The request data.
   */
  protected function setRequestBody($request_body) {
    $this->requestBody = $request_body;
  }

};
