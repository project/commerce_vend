<?php
/**
 * @file
 * An object representing a Vend API response.
 */

/**
 * Class CommerceVendResponse
 */
class CommerceVendResponse {

  protected $httpResponse = FALSE;
  protected $httpStatus = FALSE;

  /**
   * Create a new CommerceVendResponse object.
   *
   * @param array $http_response
   *   The response from a drupal_http_request call.
   */
  public function __construct($http_response) {
    // Save our HTTP request.
    $this->httpResponse = $http_response;
    $this->httpStatus = isset($this->httpResponse->status_message) ? $this->httpResponse->status_message : FALSE;
  }

  /**
   * Check if our request was valid and return JSON or FALSE on failure.
   *
   * @return bool|mixed
   *   A json decoded response from the API.
   */
  public function getResponse() {
    return isset($this->getRawHttpResponse()->data) ? drupal_json_decode($this->getRawHttpResponse()->data) : FALSE;
  }

  /**
   * Check if the request was a success.
   */
  public function wasSuccess() {
    $response = $this->getResponse();
    $http_was_success = $this->httpStatus == 'OK';
    $api_call_success = !(isset($response['status']) && $response['status'] == 'error');
    return $http_was_success && $api_call_success;
  }


  /**
   * Get an error associated with a response from the API.
   */
  public function getError() {
    $response = $this->getResponse();
    $error = FALSE;
    if ($this->httpStatus != 'OK') {
      $error = 'HTTP Error';
    }
    else {
      if (isset($response['error']) && !empty($response['error'])) {
        $error = $response['error'];
      }
      if (isset($response['details']) && !empty($response['details'])) {
        $error .= ': ' . $response['details'];
      }
    }
    return $error;
  }


  /**
   * Get the raw http response.
   *
   * @return bool
   *   Our raw HTTP response.
   */
  public function getRawHttpResponse() {
    return $this->httpResponse;
  }

}
