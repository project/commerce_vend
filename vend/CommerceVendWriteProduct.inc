<?php
/**
 * @file
 * An request object for updating and creating Vend products.
 */

/**
 * Class CommerceVendWriteProduct
 */
class CommerceVendWriteProduct extends CommerceVendRequest {

  /**
   * Write a product to the vend database.
   *
   * @param array $product_attributes
   *   An array of product attributes described at:
   *   http://docs.vendhq.com/api/0.x/products.html
   */
  public function __construct($product_attributes) {

    // Set our endpoint info.
    $this->setRequestMethod('POST');
    $this->setRequestEndpoint('/api/products');

    // Set our attributes for product creation.
    $this->setRequestBody($product_attributes);
  }
};
