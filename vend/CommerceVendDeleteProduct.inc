<?php
/**
 * @file
 * An request object for deleting Vend products.
 */

/**
 * Class CommerceVendDeleteProduct
 */
class CommerceVendDeleteProduct extends CommerceVendRequest {

  /**
   * Delete a product from Vend.
   *
   * @param string $product_id
   *   The product ID to delete
   */
  public function __construct($product_id) {
    $this->setRequestMethod('DELETE');
    $this->setRequestEndpoint('/api/products/' . $product_id);
  }

};
