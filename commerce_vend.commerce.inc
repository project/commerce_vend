<?php


/**
 * @file
 * Respond to the commerce_product hooks and take appropriate action. This is
 * not included in /inc because it responds to the commerce hooks which are
 * defined in hook_hook_info to be part of the "commerce" group.
 */


/**
 * Implements hook_commerce_product_update().
 */
function commerce_vend_commerce_product_update($product) {
  commerce_vend_sync_product($product);
}


/**
 * Implements hook_commerce_product_insert().
 */
function commerce_vend_commerce_product_insert($product) {
  commerce_vend_sync_product($product);
}


/**
 * Implements hook_commerce_product_delete().
 */
function commerce_vend_commerce_product_delete($product) {
  commerce_vend_delete_remote_product($product);
}
